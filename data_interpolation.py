
from ast import Try
from calendar import c
import enum
import os
from weakref import ref
import pandas as pd 
import openpyxl
import numpy as np
import random
import torch
seed=10
random.seed(seed)
np.random.seed(seed)
torch.manual_seed(seed)

import re
from collections import deque, defaultdict, OrderedDict
from unicodedata import normalize
import itertools
import math
import seaborn as sns
import torch
from kde_model import KDENet,  train, build_df
import matplotlib
import matplotlib.pyplot as plt
plt.rcParams['font.family'] = 'IPAexGothic'
import numpy as np
plt.style.use('seaborn')
import japanize_matplotlib
import concurrent.futures

seed=10
random.seed(seed)
np.random.seed(seed)
torch.manual_seed(seed)


curr_dir = os.path.realpath('.')

filename =  os.path.join(curr_dir, "data", "MASTER","最終的に欲しいバックデータ形式_230228処理結果-v1-補正.xlsx")

import datetime
curr_time = datetime.datetime.strftime(datetime.datetime.now(), "%Y-%m-%d %H:%M:%S") 
SIM_PATH = "./data/MASTER/simulation_test"
SAVEPATH = f"{SIM_PATH}/model_{curr_time}"

# os.makedirs(SAVEPATH, exist_ok=True)
os.makedirs(SIM_PATH, exist_ok=True)


def read_excel_file() ->pd.DataFrame:
    names = ['燃費_平均値(平均)', '燃費_平均値(中央)', '燃費_中央値(平均)', '燃費_中央値(中央)']
    dfs = dict()
    for sheet in list(map(lambda co2_name: f'最終的に求めたいバックデータ形式({co2_name})補正',
                    names)):
        df = pd.read_excel(filename, sheet_name=sheet, skiprows=0) 
        top_cols = [col for col in df.columns if  'Unnamed' not in col]
        ref_col = None
        df.drop(columns=["Unnamed: 0"], inplace=True)
        columns = []
        for i, (n1, n2) in enumerate(df.iloc[0].items()):
            if "Unnamed" not in n1:
                ref_col = n1
            columns.append((ref_col, n2))
        df.columns = columns
        df = df.iloc[2:]
        dfs[sheet] = df
    return dfs

def group_df_data_by_car(df, colname):
    car_types = np.unique([x[0] for x in df.index.values[:,]])
    box_types = np.unique([x[1] for x in df.index.values[:,]])
    engines = np.unique([x[1] for x in df.columns[:]])
    cols = [ (colname, et) for et in engines]
    res = OrderedDict()
    
    for ct in car_types:
        _df = df.query(f'メーカー == "{ct}"')[cols].copy()

        for col in engines:
            _df[("mask", col)] = 0.
        
        for col in _df.columns:
            if colname in col[0]:
                mask = []
                for i, val in enumerate(_df[col]):
                    if str(val).lower() != "nan": continue
                    mask.append(i)
                for index in mask:
                    _df[col][index] = 0.0
                    # _df.loc[_df.index[index], col] = 0.0
        
        _df = _df.drop(columns=[ (colname, '燃料電池'),
                        (colname, '電気'),
                        ("mask",  '燃料電池'),
                        ("mask", '電気'),
                        ])
        for bt in box_types:
            if (ct, bt) not in _df.index:
                _df.loc[(ct, bt), :] = 0.
        _df = _df.reset_index()
        _df = _df.sort_values(["メーカー", "ボディタイプ"]).reset_index(drop=True)
        for et in engines:
            if ("mask", et) in _df.columns:
                val = _df[(colname, et)] > 0.0
                val = val.values.astype(float)
                _df[("mask", et)] = val
        res[ct] = _df
        
    return res
 

def get_training_inputs(dfs, training_col, epsilon=1e-9):
    targets = []
    inputs = []
    engines =  dict()
    j = 0

    for col in dfs["BMW"].columns:
        if training_col in col[0]:
            engines[col[1]] = j
            j += 1 
    for i, (k, df) in enumerate(dfs.items()):
        for id2, col in enumerate(df.columns):
            if training_col in col[0]:
                target = df[(training_col, col[1])]  * df[("mask", col[1])]
                target = target.values
                indices = np.argwhere(target > 0.0).reshape(-1,)
                if not len(indices):
                    continue
                for  id1 in indices:
                    inputs.append((i, id1, engines[col[1]]))
                    targets.append(target[id1])
    targets = np.stack(targets)
    inputs = np.stack(inputs)
    return inputs, targets, engines


def main():
    dfs = read_excel_file()
    # print(dfs.keys())
    for sheet, df in dfs.items():
        df = df.copy(deep=True)
        df2 = df.set_index([( '走行距離(km)', 'メーカー'), 
                            ( '走行距離(km)', 'ボディタイプ'),
                ])

        # training_col = 'CO2排出量(g-CO2/km)_old'
        training_col = "走行距離(km)_old"
        df2.index.set_names([ 'メーカー', 'ボディタイプ'], inplace=True)
        r = group_df_data_by_car(df2, training_col)
        inputs, targets, _ = get_training_inputs(r, training_col)
        target_max = {'CO2排出量(g-CO2/km)_old': 500, "走行距離(km)_old": 12000.}
        t = train(inputs, targets, target_max=target_max[training_col],
            filename=f"{training_col[:5]} - {sheet}")
        print(sheet)

def build_out_file(df):
    cols1, cols2 = set(), set()
    for (col1, col2) in df.columns:
        cols1.add(col1)
        cols2.add(col2)
        
    # print(cols1, cols2)
    cols1 = ['走行距離(km)', 
            'CO2排出量(g-CO2/km)',  
            '合計 /  SUM(台数)', 
            '走行距離(km)_old', 
            'CO2排出量(g-CO2/km)_old', 
            'distance_mask', 
            'co2_mask', 
            
            ]
    cols2 = ['メーカー', 
            'ボディタイプ', 
            '電気', 
            'プラグインハイブリッド', 
            'ハイブリッド', 
            '燃料電池', 
            'ガソリン', 
            'ディーゼル',
            'その他ガスなど',
            '不明/該当なし', 
            ]
    columns = pd.MultiIndex.from_product([cols1, cols2])  
    suppress = []
    
    out_df = pd.DataFrame(columns=columns)

    for i in range(len(df)):
        for idx1, idx2 in df.iloc[i].index:
            value = df.iloc[i][(idx1, idx2)]
            str_value = str(value).lower()
            if str_value == "nan":
                if idx1 == '走行距離(km)' and idx2 in cols2[2:]:
                    out_df.loc[i, ('distance_mask', idx2)] = 0
                if idx1 == 'CO2排出量(g-CO2/km)' and idx2 in cols2[2:]:
                    out_df.loc[i, ('co2_mask', idx2)] = 0
                continue
            out_df.loc[i, (idx1, idx2)] = value
            # if idx1 in ['走行距離(km)' , 'CO2排出量(g-CO2/km)'] and idx2 in cols2[2:]:
            #     float(value)

            if idx1 == '走行距離(km)' and idx2 in cols2[2:] and value == 0.0:
                out_df.loc[i, ('distance_mask', idx2)] = 1

            if idx1 == 'CO2排出量(g-CO2/km)' and idx2 in cols2[2:] and value == 0.0:
                out_df.loc[i, ('co2_mask', idx2)] = 1

            if idx1 == '走行距離(km)' and idx2 in cols2[2:] and value > 0.0:
                out_df.loc[i, ('distance_mask', idx2)] = 2

            if idx1 == 'CO2排出量(g-CO2/km)' and idx2 in cols2[2:] and value > 0.0:
                out_df.loc[i, ('co2_mask', idx2)] = 2

    for i, col in enumerate(out_df.columns):
        if col[0] != '走行距離(km)' and (col[1] == 'メーカー'or col[1] == 'ボディタイプ'):
            suppress.append(col)
    
    out_df.drop(columns=suppress, inplace=True)
    
    

    return out_df


def run_evaluation(car, box, engine, net, mu):
    import torch.nn.functional as F
    num_car=17
    num_box=25
    num_eng=6

    cars = torch.tensor([car])
    boxes = torch.tensor([box])
    engines = torch.tensor([engine])

    cars = F.one_hot(cars, num_car).to(torch.float32)
    boxes = F.one_hot(boxes, num_box).to(torch.float32)
    engines = F.one_hot(engines, num_eng).to(torch.float32)
    with torch.no_grad():
        out, feat = net(cars, boxes, engines, mu)
    return out


def fill_with_zeros(out_df):
    for col in [( '走行距離(km)', '電気'), 
                ( '走行距離(km)', '燃料電池'),
                ('CO2排出量(g-CO2/km)','電気'),
                ( 'CO2排出量(g-CO2/km)','燃料電池'),
                ]:
        
        out_df.loc[:, col] = 0.

    return out_df


def modify_df_format(out_df_style ):
    for col in out_df_style.columns:
        if col[0] in ['走行距離(km)', 'CO2排出量(g-CO2/km)', '合計 /  SUM(台数)', 
                        'distance_mask', 'co2_mask'
        ]:
            continue
        # print(col)
        out_df_style.data.drop(columns=[col], inplace=True)
    

    return out_df_style


def evaluate(ncluster=5, sigma=1., cluster_dim=10, savepath=None):
    if savepath is None:
        savepath = SAVEPATH
    os.makedirs(savepath, exist_ok=True)
    dfs = read_excel_file()
    target_max = {'CO2排出量(g-CO2/km)_old': 500., "走行距離(km)_old": 12000.}
    masks = {'走行距離(km)': 'distance_mask', 'CO2排出量(g-CO2/km)':'co2_mask'}
    
    res_df = dict()
    for sheet, df in dfs.items():
        df = df.copy(deep=True)
        out_df = build_out_file(df)
        df2 = df.set_index([( '走行距離(km)', 'メーカー'), 
                            ( '走行距離(km)', 'ボディタイプ'),
                ])
        df2.to_excel("./test.xlsx")
        
        df2.index.set_names([ 'メーカー', 'ボディタイプ'], inplace=True)
        for training_col in ["走行距離(km)_old", 'CO2排出量(g-CO2/km)_old']:
            random.seed(seed)
            np.random.seed(seed)
            torch.manual_seed(seed)
            ref_col = training_col.replace('_old', '')
            r = group_df_data_by_car(df2.copy(deep=True), training_col)
            inputs, targets, engines = get_training_inputs(r, training_col)
            _df = r["BMW"]
            cars = {name: i for i, name in enumerate(r.keys())}
            body_types = { b:i for i, b in enumerate(_df['ボディタイプ'].values)}
            t = train(inputs, targets, target_max=target_max[training_col], save_dir=savepath,
            filename=f"{training_col[:5]} - {sheet}", seed=seed, 
            ncluster=ncluster, sigma=sigma, cluster_dim=cluster_dim)

            net = t["net"]
            mu = t["mu"]
            for j in range(len(out_df)):
                car, body = out_df[out_df.columns[0]][j], out_df[out_df.columns[1]][j]
                car, body = cars[car], body_types[body]
                cols = out_df.columns
                for k, eng in enumerate(out_df.iloc[j]):
                    if k < 2:
                        continue
                    if cols[k][0] == ref_col:
                        if cols[k][1] not in engines:
                            continue
                        engine = engines[cols[k][1]]
                        curr_val = out_df.iloc[j][k]
                        # print(curr_val, cols[k][1])
                        if str(curr_val).lower() == "nan" or curr_val == 0.0:
                            if str(curr_val).lower() == "nan":
                                color = "red"
                                # out_df.loc[j, (masks[ref_col], cols[k][1]) ] = 0
                            else:
                                color = "green"
                                # out_df.loc[j, (masks[ref_col], cols[k][1]) ] = 1
                            out = run_evaluation(car, body, engine, net, mu)
                            res = (out[0] * target_max[training_col]).item()
                            out_df.loc[j, (ref_col, cols[k][1]) ] = res
                        # else:
                            # out_df.loc[j, (masks[ref_col], cols[k][1]) ] = 2

        new_filename = os.path.join(f"{savepath}/最終的に欲しいバックデータ形式_230302処理結果-v1-補正.xlsx")
        
        # Fill skip columns
        out_df = fill_with_zeros(out_df)

        masks_df = {"distance_mask": out_df["distance_mask"].copy(),
                     "co2_mask": out_df["co2_mask"].copy(),
        }


        # Remove index
        # index = out_df[[( '走行距離(km)', 'メーカー'), (  '走行距離(km)', 'ボディタイプ')]].values
        # print(index)
        
        # out_df.set_index([( '走行距離(km)', 'メーカー'),
        #                   (  '走行距離(km)', 'ボディタイプ'),], inplace=True)

        # print(out_df.index.names[0] == ('走行距離(km)', 'メーカー'))

        # out_df = out_df.rename_axis(index={('走行距離(km)', 'メーカー'): None,
        #                                     ('走行距離(km)', 'ボディタイプ'): None
        
        # })
        # print(out_df.index.names)

        # out_df.drop(columns=out_df.columns[0:2], inplace=True)
        # out_df.index = pd.MultiIndex.from_tuples(index.squeeze().tolist())
        # print(out_df.index)
        # print(out_df)
        # out_df = out_df.reset_index()
        # out_df.index.names = [None]
        # out_df.to_excel('./data/MASTER/test.xlsx')

        # raise Exception
        # out_df.index = index[:, 0]
        # print(index.squeeze().tolist())
        # index = index.squeeze().tolist(),

        # out_df.index = pd.MultiIndex.from_arrays()
        # out_df.index = index1
        # out_df.index.set_names([ ' ', '  '], inplace=True)
        # print(out_df.columns[0])
        # out_df = out_df.rename(columns={
        #     out_df.columns[0]: 'd',
        #   out_df.columns[1]: 'k', 
        # })

        # print(out_df.columns)

        

        # print(masks_df["distance_mask"])
        out_df_style = build_df(out_df, keys=masks, masks_df=masks_df)

        out_df_style = modify_df_format(out_df_style )

        try:
            with pd.ExcelWriter(new_filename, engine='openpyxl', mode='a') as writer: 
                out_df_style.to_excel(writer, sheet_name=f'{sheet}')
        except:
            with pd.ExcelWriter(new_filename, engine='openpyxl', mode='w') as writer: 
                out_df_style.to_excel(writer, sheet_name=f'{sheet}')


def run_fn():
    pass

def run_models_evaluation():

    def execute():
        pass

    # clusters = [5, 8, 10, 15, 18, 20]
    clusters = [5, ]
    # sigmas = [0.5, 0.8, 1.0, 1.5, 2.0, 5.0]
    sigmas = [0.5, ]
    cluster_dim_size = [10]
    hyperparameters = list(itertools.product(clusters, sigmas, cluster_dim_size))

    for ncluster, sigma, clus_dim in hyperparameters:
        evaluate( ncluster, sigma, clus_dim, f"{SIM_PATH}/model_{ncluster}-{sigma}-{clus_dim}")

    # print(len(hyperparameters))
    # with concurrent.futures.ProcessPoolExecutor(max_workers=8) as ex:
    #     futures = {ex.submit(evaluate, ncluster, sigma, clus_dim, f"{SIM_PATH}/model_{ncluster}-{sigma}-{clus_dim}"): f"{ncluster}-{sigma}-{clus_dim}"
    #                for ncluster, sigma, clus_dim in hyperparameters
    #     }
    #     for future in concurrent.futures.as_completed(futures):
    #         print('Finished ', future.result())


if __name__ == "__main__":
    # evaluate()
    run_models_evaluation()

