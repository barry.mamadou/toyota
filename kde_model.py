from re import S
import torch
from torch import ge, nn
import torch.nn.functional as F
import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd
import random
# curr_dir = "/home/barry/projects/nexco/toyota"


class GaussianKernel(nn.Module):
    def __init__(self, dim, sigma):
        super().__init__()
        self.sigma = sigma
        self.dim = dim
        self.cov = torch.eye(dim).to(dtype=torch.float32) * self.sigma
        
    def forward(self, x, mu):
        x = x.unsqueeze(dim=1)
        prod = ( (x - mu) * (x-mu) ).sum(dim=2)
        prod = -0.5 * prod / self.sigma
        return torch.exp(prod)

class KDENet(nn.Module):
    
    def __init__(self, num_car=17, num_box=25, num_eng=6, ncluster=5, sigma=1.0, cluster_dim=10):
        super().__init__()
        self.net = nn.Linear(num_car + num_box + num_eng, 32)
        self.net1 = nn.Linear(32, cluster_dim)
        self.net2 = nn.Linear(32, ncluster)
        
        self.mu = nn.Linear(32, num_car + num_box + num_eng)
        
        self.kernel = GaussianKernel(num_car, sigma=sigma)
        self.leaky_rul = nn.LeakyReLU(0.2)
        self.sigma = sigma
        self.ncluster = ncluster
        self.cluster_dim = cluster_dim

    def forward(self, cars, boxes, engines, mu):
        x = torch.concat([cars, boxes, engines], dim=1)
        x = nn.ReLU()(self.net(x))
        feat = nn.ReLU()(self.net1(x))
        
        y = nn.ReLU()(self.net2(x))
        x = self.leaky_rul(self.kernel(feat, mu))
        out = x * y
        out = nn.ReLU()(out.sum(dim=1))
        return out, feat
        

def knn(feat, mu, net):
    with torch.no_grad():
        # x = torch.square(feat.unsqueeze(1) - mu)
        # x = torch.sum(x, dim=-1)
        dst = net.kernel(feat, mu)
        args = torch.argmin(dst, dim=1)
        mus = []
        for  i in range(mu.shape[1]):
            idx = torch.argwhere(args == i ).squeeze()
            if idx.size() and len(idx):
                _mu = torch.mean(feat[idx], dim=0)
            else:
                _mu = mu[0][i]
            mus.append(_mu)
        mus = torch.stack(mus)
        mus = torch.broadcast_to(mus, (mu.shape[0], mu.shape[1],  mus.shape[1],))
        return mus
    
def run_train(cars, boxes, engines, _targets, mu, save_dir, filename, seed, ncluster=5, 
    cluster_dim=10, sigma=1.0):
    random.seed(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    net = KDENet(ncluster=ncluster, sigma=sigma, cluster_dim=cluster_dim)
    loss_fn = nn.MSELoss()
    losses = []
     #TODO
    # lr = 0.001
    lr = 0.01
    optimizer = torch.optim.SGD(net.parameters(), lr=lr)
    # eps = 250000
    # TODO
    eps = 500000
    for i in range(eps):
        optimizer.zero_grad()
        out, feat = net(cars, boxes, engines, mu)
        l = loss_fn(out, _targets)
        losses.append(l.item())
        l.backward()
        optimizer.step()
        if not i % 10000:
            print(f"Epoch {i}, loss {l.item()}")
        mu = knn(feat, mu, net)
    torch.save({"params": net.state_dict(), "losses": np.array(losses), "mu": mu[0]},
        os.path.join(save_dir , f'{filename}.pkl'))
    plt.plot(losses)
    plt.savefig(os.path.join(save_dir , f'{filename}.png'))
    return {"losses": losses, "mu": mu, "net": net, "eps": eps, "lr": lr, "ncluster": ncluster,
    "cluster_dim": cluster_dim, "sigma": sigma}


def train(inputs, targets, target_max, save_dir, filename, seed, ncluster=5, cluster_dim=10, sigma=1.0):
    num_car, num_box, num_eng = 17, 25, 6
    inputs = torch.tensor(inputs)
    cars, boxes, engines = torch.tensor_split(inputs,3, dim=1)
    cars = torch.squeeze(cars)
    boxes= torch.squeeze(boxes)
    engines = torch.squeeze(engines)
    cars = F.one_hot(cars, num_car).to(torch.float32)
    boxes = F.one_hot(boxes, num_box).to(torch.float32)
    engines = F.one_hot(engines, num_eng).to(torch.float32)
    targets = torch.from_numpy(targets).to(torch.float32)
    # target_max = 350.
    targets = targets / target_max
    
    # return cars, boxes, engines, targets / target_max
    mu = torch.randn( (ncluster, cluster_dim,), )
    mu =  torch.broadcast_to(mu, (cars.shape[0], ncluster, cluster_dim))

    return run_train(cars, boxes, engines, targets, mu, save_dir, filename, seed,
        ncluster, cluster_dim, sigma)
    

def build_df(df, keys, masks_df):
    df = df.copy(deep=True)
    colors = {0: 'background-color: red', 1:'background-color: green', 2: 'background-color: white',
                 -1: 'background-color: white'}
    def color(x):
        colname, engine_type = x.name
        skip_columns = ["電気", "燃料電池",  'メーカー', 'ボディタイプ']
        if engine_type in skip_columns:
            return [''] * len(x)

        if colname in keys:
            # mask_colname = (keys[colname], engine_type)
            masks = masks_df[keys[colname]][engine_type]
            # masks = df[mask_colname].values
            return [colors[masks[i]] for i in range(len(x)) ]
        return [''] * len(x)
        
    _df = df.style.apply(color)

    return _df