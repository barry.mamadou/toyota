
import os

from dash import Dash, html, dcc
import plotly.express as px
import pandas as pd


datapath = os.path.join(os.path.realpath("."), "data/CO2簡易計算シート_221216/")

filename = os.path.join(datapath, "CO2簡易計算結果.xlsx")
df = pd.read_excel(filename, sheet_name="メーカーブランド")

# print(df)

# fig = px.bar(df, x="Fruit", y="Amount", color="City", barmode="group")

def generate_table(df, max_rows=10):
    return html.Table([
        html.Thead(
            html.Tr([html.Th(col) for col in df.columns])
        ),
        html.Tbody([
            html.Tr([
                html.Td([
                    # html.Label('Text Input'),
                    dcc.Input(value=df.iloc[i][col], type='text'),
                    ]) for col in df.columns
            ]) for i in range(min(len(df), max_rows))
        ])
    ])

# external JavaScript files
external_scripts = [
    'https://www.google-analytics.com/analytics.js',
    {'src': 'https://cdn.polyfill.io/v2/polyfill.min.js'},
    {
        'src': 'https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.10/lodash.core.js',
        'integrity': 'sha256-Qqd/EfdABZUcAxjOkMi8eGEivtdTkh3b65xCZL4qAQA=',
        'crossorigin': 'anonymous'
    }
]

external_stylesheets = [
    'https://codepen.io/chriddyp/pen/bWLwgP.css',
    {
        'href': 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css',
        'rel': 'stylesheet',
        'integrity': 'sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO',
        'crossorigin': 'anonymous'
    }
]

app = Dash(__name__, external_scripts=external_scripts,
                external_stylesheets=external_stylesheets)


app.layout = html.Div(children=[
    html.H1(children='Dash Sample'),
    html.P(children="Car Brand Summary Table", style={"fontSize": 24}),
    generate_table(df),
    html.Button(children="Run Simulation", style={"fontSize": 24, "marginTop": 10, "backgroundColor": "#007eff", "color": "white", "border": "8px"})
    # dcc.Graph(
    #     id='example-graph',
    #     figure=fig
    # )
    
], style={"backgroundColor": "#0e1012", "color": "#FFFFFF"})



if __name__ == '__main__':
    app.run_server(debug=True)
